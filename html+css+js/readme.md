В папке представены два неболших учебных проекта.
При выполнении использовались html, css и JavaScript.

1) В проекте (modal window) сделаны карточки продуктов. 
При нажатии на кнопки вылазять модальные окна.
Одно показывает цену. С помощью другого можно удалить карточку продукта.

2) В проекте (table) реализована отрисовка html таблицы из JS.
Создан класс table, который по входным данным отрисовывает таблицу,
стилизует и вставляет его в dom. 
Данные приходят с локального json server: https://github.com/typicode/json-server
Запускаемого на компьютере. 
Данный проект еще не завершен, планируется добавить блок select элементов,
с помощью которых можно будет выбирать записи из таблицы по критериям указанным в селектах.


P.S.
Не уверен в красоте и выразительности моего JS кода,
если вы готовы указать на недостатки и дать совет, то 
буду рад с вами побеседовать, мой телеграм: @shahimsafin.
